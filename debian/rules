#!/usr/bin/make -f
# GNU copyright 1997 to 1999 by Joey Hess.
# copyright 2000 Daniel Burrows

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

# Files modified by the build process
KEEP_FILES = \
 gui/Makefile.in \
 common/Makefile.in \
 client/levels/Makefile.in \
 client/gfx/Oz/Makefile.in \
 client/gfx/Moiree/Makefile.in \
 client/gfx/Makefile.in \
 client/gfx/Classic/Makefile.in \
 client/gfx/AbsoluteB/Makefile.in \
 client/gui_theme/Makefile.in \
 client/sounds/Makefile.in \
 client/Makefile.in \
 server/levels/Makefile.in \
 server/Makefile.in \
 config.guess \
 config.h.in \
 config.sub \
 configure \
 game/Makefile.in \
 Makefile.in \
 docs/Makefile.in \
 m4/Makefile.in

# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
include /usr/share/dpkg/architecture.mk

configure: configure-stamp
configure-stamp:
	dh_testdir
	for f in $(KEEP_FILES); do [ -f $$f.orig ] || cp -a $$f $$f.orig ; done
	cp -f /usr/share/misc/config.sub config.sub
	cp -f /usr/share/misc/config.guess config.guess

	CFLAGS="$(CFLAGS)" ./configure --host=$(DEB_HOST_GNU_TYPE) --build=$(DEB_BUILD_GNU_TYPE) \
	--prefix=/usr --bindir=\$${prefix}/games --mandir=\$${prefix}/share/man \
	--infodir=\$${prefix}/share/info --with-highscore-path=/var/games --with-doc-path=\$${prefix}/share/doc \
	--datadir=/usr/share/games --localstatedir=/var/games --with-docdir=/usr/share/doc

	touch configure-stamp


build-arch: build
build-indep: build

build: build-stamp
build-stamp: configure-stamp
	dh_testdir

	$(MAKE)

	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp configure-stamp install-stamp

	[ ! -f Makefile ] || $(MAKE) distclean
	rm -f lbreakout2.desktop

	for f in $(KEEP_FILES); do [ ! -f $$f.orig ] || mv $$f.orig $$f; done
	dh_clean

install: install-stamp
install-stamp: build-stamp
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs

	$(MAKE) install DESTDIR=`pwd`/debian/lbreakout2
	mkdir -p debian/lbreakout2-data/usr/share/doc/lbreakout2-data

	dh_install -plbreakout2-data
	rm -fr debian/lbreakout2/usr/share/games/lbreakout2

	mkdir debian/lbreakout2/usr/share/games/lbreakout2
	mv `pwd`/debian/lbreakout2/var/games/lbreakout2.hscr `pwd`/debian/lbreakout2/usr/share/games/lbreakout2/lbreakout2.hscr.blank

	mkdir -p debian/lbreakout2/usr/share/pixmaps
	cp debian/lbreakout2.xpm debian/lbreakout2/usr/share/pixmaps
	cp debian/lbreakout2.desktop debian/lbreakout2/usr/share/applications
#	Remove some files duplicated by the upstream build system
	rm -fr debian/lbreakout2-data/usr/share/games/locale
	rm -fr debian/lbreakout2-data/usr/share/games/applications
	rm -fr debian/lbreakout2-data/usr/share/games/icons
	rm -fr debian/lbreakout2/usr/share/games/applications
	rm -fr debian/lbreakout2/usr/share/games/icons

	touch install-stamp

binary-indep: build install
	dh_testdir -i
	dh_testroot -i
	dh_installdocs -i -plbreakout2-data
	dh_installchangelogs -i -a ChangeLog
	dh_compress -i
	dh_fixperms -i

	dh_installdeb -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i

binary-arch: build install
	dh_testdir -a
	dh_testroot -a
	dh_installdocs -plbreakout2
	dh_installmenu -a
	dh_installman -a debian/lbreakout2.6 debian/lbreakout2server.6
	dh_installchangelogs -a ChangeLog
	dh_strip -a
	dh_compress -a
	dh_fixperms -a

	chown root:games $(CURDIR)/debian/lbreakout2/usr/games/lbreakout2
	chmod g+s $(CURDIR)/debian/lbreakout2/usr/games/lbreakout2

	dh_installdeb -a
	dh_shlibdeps -a
	dh_gencontrol -a
	dh_md5sums -a
	dh_builddeb -a

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install configure
